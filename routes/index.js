'use strict';

var express = require('express');
var router = express.Router();
var fs = require('fs');
var imgSize = require('image-size');

/* GET INDEX page. */
router.get('/', function(req, res) {
  res.render('index');
});

/* GET EDIT page. */
router.get('/edit', function(req, res) {
  res.render('edit');
});

/* POST image file. */
router.post('/upload', function(req, res) {
	var imagePath = '/uploads/' + req.files[Object.keys(req.files)[0]].name;
	var dimensions = imgSize('public/' + imagePath);

  res.send({
  	fileDir: imagePath,
  	width: dimensions.width,
  	height: dimensions.height,
  	name: req.files[Object.keys(req.files)[0]].originalname
  });
});

module.exports = router;
