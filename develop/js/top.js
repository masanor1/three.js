'use strict';

var $ = require('jquery');
var _ = require('underscore');

var $win = $(window);
var $logo = $('#logo');
var $field = $('#field');
var width = $field.width();
var height = $field.height();
// x=前後, y=上下,  z=左右
$logo.css({
  left: (width - $logo.width()) / 2,
  top: 10
});

// /* 円周率を角度に変換する */
// var PItoAngle = function (PI) {
//   // 一回転以上している場合があるので、その場合は正しい値に戻す
//   var num = PI;

//   var rotationsNum = Math.abs(Math.floor(num / (Math.PI * 2)));

//   // もし負の値ならば正の値に変換する
//   if (PI < 0) {
//     num = PI + (Math.PI * 2) * (rotationsNum + 1);
//   }

//   // 一回転以上の値を通常の角度の値に戻す
//   var overNum = num - (Math.PI * 2) * rotationsNum;
//   var unit = 360 / (Math.PI * 2);

//   return unit * overNum;
// }

/** シーンの生成 **/
var scene = new THREE.Scene();
scene.fog = new THREE.FogExp2(0xccccff, 0.03);

/** カメラの生成 **/
// 視野角・アスペクト比・最小描画範囲・最大描画範囲
var camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000);
// x軸, y軸, z軸
camera.position.set(0, 14.5, 85);
camera.rotation.x += 0.1;
// カメラの表示範囲
camera.aspect = 600 / 400;

/** ライトの作成 **/
// スポットライト
var spotLight = new THREE.SpotLight(0xffffff, 2, 130, 0.5, 18);
spotLight.target.position.set(0, 0, 40);
spotLight.position.set(0, 60, 90);
spotLight.castShadow = true;
spotLight.shadowBias = -0.0001;
spotLight.shadowMapWidth = spotLight.shadowMapHeight = 2048;
spotLight.shadowDarkness = 0.4;

// スポットライト2
var spotLight2 = new THREE.SpotLight(0xffffff, 0.7, 130);
spotLight2.target.position.set(0, 0, 40);
spotLight2.position.set(0, 60, 40);
spotLight2.castShadow = true;
spotLight2.shadowBias = -0.0001;
spotLight2.shadowMapWidth = spotLight2.shadowMapHeight = 1200;
spotLight2.shadowDarkness = 0.7;

//環境光
var ambientLight = new THREE.AmbientLight(0x444433);

// 空の光
var hemiLight = new THREE.HemisphereLight( 0x7777ff, 0xcccccc, 0.3 ); 

scene.add(hemiLight);
scene.add(spotLight);
scene.add(spotLight2);
scene.add(ambientLight);  

/** でこぼこした地面を作る **/
var simplexNoise = new SimplexNoise();
var map1 = THREE.ImageUtils.loadTexture( '/images/suna.jpg' );
var pGeometry = new THREE.PlaneGeometry(190, 190, 300, 300);

// ジオメトリをでこぼこさす
for ( var i = 0; i < pGeometry.vertices.length; i++ ) {
    var vertex = pGeometry.vertices[i];
    vertex.z = simplexNoise.noise(vertex.x / 25, vertex.y / 25);
}

// でこぼこの影を作る
pGeometry.computeFaceNormals();
pGeometry.computeVertexNormals();

var plane = new THREE.Mesh(
  pGeometry, 
  new THREE.MeshPhongMaterial({
    map: map1,
    bumpMap: map1,
    bumpScale: 5,
    shininess: 0,
    ambient:0xffffff
  })
);

plane.position.set(0, 11.5, -10);
plane.rotation.x = -90 * Math.PI / 180;
plane.receiveShadow = true; 

scene.add(plane);

/** 3Dモデルを読み込む **/
var model_load = '/model/house/house.obj';
var material_load = '/model/house/house.mtl';
var loader = new THREE.OBJMTLLoader();
var house;

loader.load(model_load, material_load, function ( object ) {

  object.position.set(0, 11.7, 35);
  object.receiveShadow = true;
  object.castShadow = true;
  object.rotation.y = Math.PI;
  scene.add(object);

  object.scale.x = 0.8;
  object.scale.y = 0.8;
  object.scale.z = 0.8;

  object.traverse(function(node) {
    if (node instanceof THREE.Mesh) {
      node.castShadow = true;
      node.receiveShadow = true;
      node.ambient = 0xffffff;
    }
  });

  house = object;
  // 家が読み込み終わったあたりで霧を薄くする
  fadeOutFog();
});

// var doorGeo = new THREE.BoxGeometry(10, 15, 1);
// var doorMaterial = new THREE.MeshLambertMaterial({color: 0xff0000, side: THREE.DoubleSide, ambient: 0xffffff});

// var door = new THREE.Mesh(doorGeo, doorMaterial);
// // 影をON
// door.castShadow = true;
// door.receiveShadow = true;
// door.position.set(0, 16, 0);
// scene.add(door);

// $('h2').on('click', function() {
//   open();
// });
// function open() {  
//   if (door.rotation.y >= -Math.PI / 2 || door.position.x >= -4 || door.position.z <= +4) {
//     requestAnimationFrame(open);
//   }

//   if (door.rotation.y >= -Math.PI / 2) {
//     door.rotation.y -= 0.15;
//   }

//   if (door.position.x >= -4) {
//     door.position.x -= 0.3;
//   }

//   if (door.position.z <= +5) {
//     door.position.z += 0.35;
//   }
// }

var cosmoMap = new THREE.ImageUtils.loadTexture('/images/cosmo5.jpg');
var cosmo = new THREE.Mesh(
  new THREE.SphereGeometry(120, 300, 300),
  new THREE.MeshLambertMaterial({
    map: cosmoMap,
    bumpMap: cosmoMap,
    bumpScale: 3,
    side: THREE.DoubleSide,
    ambient: 0xffffff 
  })
);
cosmo.scale.y = 0.15;
cosmo.scale.x = 0.95;
cosmo.scale.z = 1;

cosmo.position.set(0, 14, 0);
scene.add(cosmo);

/** レンダラーの生成 **/
var renderer;

// WebGLの有無を確認
if (window.WebGLRenderingContext) {
  renderer = new THREE.WebGLRenderer({ antialias: true }); 
} else {
  renderer = new THREE.CanvasRenderer({ antialias: true });
}

// 影をON
renderer.shadowMapEnabled = true; 
// 影をソフトシャドウに
renderer.shadowMapSoft = true;
renderer.shadowMapType = THREE.PCFSoftShadowMap;
// canvasサイズ設定
renderer.setSize(width, height);
// canvasサイズ設定
renderer.setClearColor(0x99aacc, 1);
// HTMLに埋め込む
$field
  .css({
    width: width,
    height: height
  })
  .html(renderer.domElement);
// レンダリング
myRender();

function myRender() {
  requestAnimationFrame(myRender);
  renderer.render(scene, camera);
};

function houseMove() {
  var animationId = requestAnimationFrame(houseMove);
  house.position.y += 0.2;

  if (house.position.y >= 14) {
    cancelAnimationFrame(animationId);
    return;
  }
}

var fogAnimationId;
/* 霧を徐々に薄くする */
function fadeOutFog() {
  if (scene.fog.density >= 0.008) {
    fogAnimationId = requestAnimationFrame(fadeOutFog);
    scene.fog.density -= 0.001;
  } else {
    cancelAnimationFrame(fogAnimationId);
  }
}

// マウスの動きに合わせてカメラを移動させる
document.addEventListener('mousemove', function(e) {
  var winHalfSize = $win.width() / 2;
  var mousePositionX = (e.clientX - winHalfSize) / 60;

  camera.position.x = mousePositionX;
  cosmo.position.x = mousePositionX;
});