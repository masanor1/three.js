// 地面の作り方

/** でこぼこした地面を作る **/
var simplexNoise = new SimplexNoise();
var map1 = THREE.ImageUtils.loadTexture( '/images/plane.jpg' );
var pGeometry = new THREE.PlaneGeometry(1000, 1000, 64, 64);

// ジオメトリをでこぼこさす
for ( var i = 0; i < pGeometry.vertices.length; i++ ) {
    var vertex = pGeometry.vertices[i];
    vertex.z = simplexNoise.noise( vertex.x * 14, vertex.y * 5 );
}

// でこぼこの影を作る
pGeometry.computeFaceNormals();
pGeometry.computeVertexNormals();

// var pMaterial = new THREE.MeshPhongMaterial({color: 0xffffff, side: THREE.DoubleSide});
var pMaterial = new THREE.MeshPhongMaterial({map: map1, side: THREE.DoubleSide});
var plane = new THREE.Mesh(pGeometry, pMaterial);

plane.position.set(0, 0, 0);
plane.rotation.x = 90 * Math.PI / 180;
scene.add(plane);
// 影をON
plane.receiveShadow = true; 