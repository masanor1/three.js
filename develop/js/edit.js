'use strict';

var $ = require('jquery');
var _ = require('underscore');

var $win = $(window);
var $doc = $(document);
var $field = $('#field');
var $3dTextInput = $('#js-3dText');
var $managerView = $('#js-objectManager');
var IMAGE_PATH = '/upload';
var workObject = 1;
var worksImage;
var width = $field.width();
var height = $field.height();
/** シーンの生成 **/
var scene = new THREE.Scene();
/** カメラの生成 **/
// 視野角・アスペクト比・最小描画範囲・最大描画範囲
var camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000);
// x軸, y軸, z軸
camera.position.set(0, 35, 80);
// カメラの表示範囲
camera.aspect = 600 / 400;

/* オブジェクトマネージャ */
var objectManager = {
	$managerView: $managerView,

	objectList: {},

	selected: null,

	line: {
		material: null,
		geometry: null,
		mesh: null
	},

	ObjListTamplate: _.template('<li class="objectInfo js-infoList"><a href="javascript:void(0)" class="hide fl"></a><div class="name fl js-infoName"><%= name %></div></li>'),

	add: function(objectInfo) {
		this.objectList[objectInfo.name] = objectInfo;

		this.addHTML(objectInfo);
	},

	addHTML: function(objectInfo) {
		this.$managerView.find('.js-objectList').append(this.ObjListTamplate(objectInfo));
	},

	getObject: function(name) {
		return this.objectList[name].object;
	},

	selectObject: function(name) {
		var object = this.getObject(name);

		this.selected = object;

		this.line.material = new THREE.LineBasicMaterial({color: 0xff0000, linewidth: 5});
		this.line.geometry = new THREE.Geometry();

		this.line.geometry.vertices.push(new THREE.Vector3(0, 500, 0), object.position);

		this.line.mesh = new THREE.Line( this.line.geometry, this.line.material );
		scene.add(this.line.mesh);
	},

	unselectObject: function() {
		this.selected = null;

		scene.remove(this.line.mesh);
	}
};

/** ライトの作成 **/
// スポットライト
var spotLight = new THREE.SpotLight(0xffffff, 0.7, 230, 1, 3);
spotLight.target.position.set(0, 0, 0);
spotLight.position.set(0, 85, 0);
spotLight.castShadow = true;
spotLight.shadowBias = -0.0001;
spotLight.shadowMapWidth = spotLight.shadowMapHeight = 2048;
spotLight.shadowDarkness = 0.4;

//環境光オブジェクトの設定　
var ambientLight = new THREE.AmbientLight(0xffffff);

// 平行光源
var light = new THREE.DirectionalLight( 0xffffff, 0.01 );
light.position.set( 0, 20, 0 );
light.target.position.copy( scene.position );
light.castShadow = true;
light.shadowBias = -0.0001;
light.shadowMapWidth = light.shadowMapHeight = 2048;
light.shadowDarkness = 0.7;

scene.add( light );
scene.add(spotLight);
scene.add(ambientLight);

// 部屋を生成
var roomGeometry = new THREE.BoxGeometry(100, 100, 180);
var roomMaterial = new THREE.MeshPhongMaterial({map: THREE.ImageUtils.loadTexture( '/images/check.jpg'), side: THREE.DoubleSide});
var room = new THREE.Mesh(roomGeometry, roomMaterial);
room.castShadow = true;
room.receiveShadow = true;
room.position.set(0, 65, 0);
scene.add(room);

/** レンダラーの生成 **/
var renderer;

// WebGLの有無を確認
if (window.WebGLRenderingContext) {
	renderer = new THREE.WebGLRenderer({ antialias: true }); 
} else {
	renderer = new THREE.CanvasRenderer({ antialias: true });
}

// 影をON
renderer.shadowMapEnabled = true; 
// 影をソフトシャドウに
renderer.shadowMapSoft = true;
renderer.shadowMapType = THREE.PCFSoftShadowMap;
// canvasサイズ設定
renderer.setSize(width, height);
// canvasサイズ設定
renderer.setClearColor(0x99aacc, 1);
// HTMLに埋め込む
$field.css({width: width, height: height}).html(renderer.domElement);
myRender();

function myRender() {
	requestAnimationFrame(myRender);
	renderer.render(scene, camera);
}


/* 画像を元に作品キャンバスを生成する */
function makeWorks(work) {
	var white = new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true, opacity: 0, shininess: 0});

	// キャンバスの縦横比
	console.log(work.width / 40);
	console.log(work.height / 40);

	var materials = [
		white,
		white,
		white,
		white,
		white,
		new THREE.MeshLambertMaterial({map: THREE.ImageUtils.loadTexture(work.fileDir), transparent: true, opacity: 0, shininess: 0})
	];
	var workGeo = new THREE.BoxGeometry(
			work.width / 40,
			work.height / 40,
			1,
			10,
			10,
			10
	);
	var workMaterial = new THREE.MeshFaceMaterial(materials);

	workObject = new THREE.Mesh(workGeo, workMaterial);
	workObject.rotation.x = 2.5;
	workObject.rotation.z = Math.PI;
	workObject.position.set(0, 25, 0);
	// 影をON
	workObject.castShadow = true;
	workObject.receiveShadow = true;

	scene.add(workObject);
	objectManager.add({
		name: work.name,
		object: workObject
	});
	fadeIn(workObject);
}

/* ふわっと出現させる */
function fadeIn() {
	if (workObject.material.materials[0].opacity !== 1) {
		requestAnimationFrame(fadeIn);
		_.forEach(workObject.material.materials, function(obj) {
			obj.opacity += 0.05;
		}); 
	}
}

/* 画像をサーバーに上げる */
function uploadFiles(url, files, callback) {
	var formData = new FormData(),
			xhr = new XMLHttpRequest(),
			file,
			i;

	for (i = 0, file; file = files[i]; ++i) {
		formData.append(file.name, file);
	}

	xhr.open('POST', url, true);
	xhr.onload = function() {
		worksImage = (new Function('return ' + xhr.response))();

		callback();
	};

	xhr.send(formData);
}

$('#js-makeText').on('click', function() {
	var textValue = $3dTextInput.val();
	var textSize = $('#js-3dTextSize').val();

	if (textValue === '') {
		textValue = $3dTextInput.attr('placeholder');
	}

	if (textSize === '') {
		textSize = 8;
	}

	//正面マテリアルの生成
	 var materialFront = new THREE.MeshBasicMaterial({color: 0xff0000, shininess: 0});

	//側面マテリアルの生成
	 var materialSide = new THREE.MeshBasicMaterial({color: 0x000088, shininess: 0});
	 var materialArray = [ materialFront, materialSide ];


	var textGeometry = new THREE.TextGeometry(
		textValue,
		{
      size: textSize, height: 4, curveSegments: 3,
      font: 'helvetiker', weight: 'bold', style: 'normal',
      bevelThickness: 1, bevelSize: 1, bevelEnabled: true,
      material: 0, extrudeMaterial: 1
		}
	);

  var textMaterial = new THREE.MeshFaceMaterial(materialArray);
  var text = new THREE.Mesh(textGeometry, textMaterial );

	// 影をON
	text.castShadow = true;
	text.receiveShadow = true;

	scene.add(text);
	objectManager.add({
		name: textValue,
		object: text
	});

	text.position.set(0, 40, 0);
});
document
	.querySelector('input[type="file"]')
	.addEventListener('change', function() {
		uploadFiles(IMAGE_PATH, this.files, function() {
			makeWorks(worksImage);
		});
	}, false);

var keyControler = {
	camera: camera,

	unitRotation: 0.03,

	unitPosition: 0.7,

	animationId: {},

	downCode: {},

	rotationState: 0,

	moveCamera: function(e) {
		var a = false;
		var actionList = [37, 38, 39, 40, 87, 68, 83, 65, 13, 16];

		_.forEach(actionList, function(code) {
			if (code === e.keyCode) {
				a = true;
			}
		});

		if (!a) {
			return;
		}

		switch(e.keyCode) {
			case 38: 
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goFront(e.keyCode);
				}

				break;
			case 40:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goBack(e.keyCode);
				}

				break;
			case 37:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goLeft(e.keyCode);
				}

				break;
			case 39:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goRight(e.keyCode);
				}

				break;
			case 87: 
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateUp(e.keyCode);
				}

				break;
			case 68:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateRight(e.keyCode);
				}

				break;
			case 83:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateDown(e.keyCode);
				}

				break;
			case 65:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateLeft(e.keyCode);
				}

				break;
			case 13:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.up(e.keyCode);
				}

				break;
			case 16:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.down(e.keyCode);
				}

				break;
		}
	},

	// 前進 38
	goFront: function() {
		keyControler.animationId[keyControler.downCode[38]] = requestAnimationFrame(keyControler.goFront);

		keyControler.camera.position.z -= keyControler.unitPosition;
	},
	// 後退 40
	goBack: function() {
		keyControler.animationId[keyControler.downCode[40]] = requestAnimationFrame(keyControler.goBack);

		keyControler.camera.position.z += keyControler.unitPosition;
	},
	// 左 37
	goLeft: function() {
		keyControler.animationId[keyControler.downCode[37]] = requestAnimationFrame(keyControler.goLeft);

		keyControler.camera.position.x -= keyControler.unitPosition;
	},
	// 右 39
	goRight: function() {
		keyControler.animationId[keyControler.downCode[39]] = requestAnimationFrame(keyControler.goRight);

		keyControler.camera.position.x += keyControler.unitPosition;
	},
	// 上を向く
	rorateUp: function() {
		if (Math.PI / 2 <= keyControler.camera.rotation.x) {
			return;
		}

		keyControler.animationId[keyControler.downCode[87]] = requestAnimationFrame(keyControler.rorateUp);
		keyControler.camera.rotation.x += keyControler.unitRotation;
	},
	// 下を向く
	rorateDown: function() {
		if (- Math.PI / 2 >= keyControler.camera.rotation.x) {
			return;
		}

		keyControler.animationId[keyControler.downCode[83]] = requestAnimationFrame(keyControler.rorateDown);
		keyControler.camera.rotation.x -= keyControler.unitRotation;
	},
	// 左を向く
	rorateLeft: function() {
		keyControler.animationId[keyControler.downCode[65]] = requestAnimationFrame(keyControler.rorateLeft);
		keyControler.camera.rotation.y += keyControler.unitRotation;
	},
	// 右を向く
	rorateRight: function() {
		keyControler.animationId[keyControler.downCode[68]] = requestAnimationFrame(keyControler.rorateRight);
		keyControler.camera.rotation.y -= keyControler.unitRotation;
	},
	// 上
	up: function() {
		keyControler.animationId[keyControler.downCode[13]] = requestAnimationFrame(keyControler.up);
		keyControler.camera.position.y += keyControler.unitPosition;
	},
	// 下
	down: function() {
		keyControler.animationId[keyControler.downCode[16]] = requestAnimationFrame(keyControler.down);
		keyControler.camera.position.y -= keyControler.unitPosition;
	},
	// 止まる
	stopCamera: function(e) {
		cancelAnimationFrame(keyControler.animationId[e.keyCode]);

		delete keyControler.animationId[e.keyCode];
		delete keyControler.downCode[e.keyCode];
	}
};


var objControler = {
	modelObject: null,

	lineObject: null,

	unitRotation: 0.03,

	unitPosition: 0.7,

	animationId: {},

	downCode: {},

	rotationState: 0,

	init: function(model, line) {
		this.modelObject = model;
		this.lineObject = line;
	},

	moveModelObject: function(e) {
		var a = false;
		var actionList = [37, 38, 39, 40, 87, 68, 83, 65, 13, 16];

		_.forEach(actionList, function(code) {
			if (code === e.keyCode) {
				a = true;
			}
		});

		if (!a) {
			return;
		}

		switch(e.keyCode) {
			case 38: 
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goFront(e.keyCode);
				}

				break;
			case 40:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goBack(e.keyCode);
				}

				break;
			case 37:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goLeft(e.keyCode);
				}

				break;
			case 39:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goRight(e.keyCode);
				}

				break;
			case 87: 
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateUp(e.keyCode);
				}

				break;
			case 68:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateRight(e.keyCode);
				}

				break;
			case 83:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateDown(e.keyCode);
				}

				break;
			case 65:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateLeft(e.keyCode);
				}

				break;
			case 13:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.up(e.keyCode);
				}

				break;
			case 16:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.down(e.keyCode);
				}

				break;
		}
	},

	// 前進 38
	goFront: function() {
		objControler.animationId[objControler.downCode[38]] = requestAnimationFrame(objControler.goFront);

		objControler.modelObject.position.z = objControler.lineObject.geometry.vertices[1].z -= objControler.unitPosition;
		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 後退 40
	goBack: function() {
		objControler.animationId[objControler.downCode[40]] = requestAnimationFrame(objControler.goBack);

		objControler.modelObject.position.z = objControler.lineObject.geometry.vertices[1].z += objControler.unitPosition;
		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 左 37
	goLeft: function() {
		objControler.animationId[objControler.downCode[37]] = requestAnimationFrame(objControler.goLeft);

		objControler.modelObject.position.x = objControler.lineObject.geometry.vertices[1].x -= objControler.unitPosition;
		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 右 39
	goRight: function() {
		objControler.animationId[objControler.downCode[39]] = requestAnimationFrame(objControler.goRight);

		objControler.modelObject.position.x = objControler.lineObject.geometry.vertices[1].x += objControler.unitPosition;
		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 上を向く
	rorateUp: function() {
		objControler.animationId[objControler.downCode[87]] = requestAnimationFrame(objControler.rorateUp);
		objControler.modelObject.rotation.x += objControler.unitRotation;
	},
	// 下を向く
	rorateDown: function() {
		objControler.animationId[objControler.downCode[83]] = requestAnimationFrame(objControler.rorateDown);
		objControler.modelObject.rotation.x -= objControler.unitRotation;
	},
	// 左を向く
	rorateLeft: function() {
		objControler.animationId[objControler.downCode[65]] = requestAnimationFrame(objControler.rorateLeft);
		objControler.modelObject.rotation.y += objControler.unitRotation;
	},
	// 右を向く
	rorateRight: function() {
		objControler.animationId[objControler.downCode[68]] = requestAnimationFrame(objControler.rorateRight);
		objControler.modelObject.rotation.y -= objControler.unitRotation;
	},
	// 上
	up: function() {
		objControler.animationId[objControler.downCode[13]] = requestAnimationFrame(objControler.up);
		objControler.modelObject.position.y = objControler.lineObject.geometry.vertices[1].y += objControler.unitPosition;
		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 下
	down: function() {
		objControler.animationId[objControler.downCode[16]] = requestAnimationFrame(objControler.down);
		objControler.modelObject.position.y = objControler.lineObject.geometry.vertices[1].y -= objControler.unitPosition;
		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 止まる
	stopModelObject: function(e) {
		cancelAnimationFrame(objControler.animationId[e.keyCode]);

		delete objControler.animationId[e.keyCode];
		delete objControler.downCode[e.keyCode];
	}
};

// オブジェクト選択 / 解除
$managerView.on('click', '.js-infoName', function() {
	var $this = $(this);

	if ($this.hasClass('is-selected')) {
		$this.removeClass('is-selected');
		objectManager.unselectObject();
		objControler.modelObject = null;

		$doc
			.off('keydown')
			.off('keyup');
		$doc
			.on('keydown', keyControler.moveCamera)
			.on('keyup', keyControler.stopCamera);

		return;
	}

	_.forEach($managerView.find('.js-infoName'), function(item) {
		var $item = $(item);

		if ($item.hasClass('is-selected')) {
				$item.removeClass('is-selected');
				objectManager.unselectObject();
				objControler.modelObject = null;

				$doc
					.off('keydown')
					.off('keyup');
				$doc
					.on('keydown', keyControler.moveCamera)
					.on('keyup', keyControler.stopCamera);
		}
	});

	$this.addClass('is-selected');
	objectManager.selectObject($(this).text());
	objControler.init(objectManager.getObject($(this).text()), objectManager.line.mesh);

	$doc
		.off('keydown')
		.off('keyup');
	$doc
		.on('keydown', objControler.moveModelObject)
		.on('keyup', objControler.stopModelObject);
});

$3dTextInput
.on('focus', function(){
	$doc.off('keydown');
})
.on('blur', function() {
	$doc.on('keydown', keyControler.moveCamera);
});

$doc
	.on('keydown', keyControler.moveCamera)
	.on('keyup', keyControler.stopCamera);
